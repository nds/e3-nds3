#
#  Copyright (c) 2018 - Present  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Joao Paulo Martins
# email   : joaopaulo.martins@ess.eu
# version : 3.2.0
#

where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile
include $(E3_REQUIRE_CONFIG)/DECOUPLE_FLAGS

#EXCLUDE_ARCHS += linux-x86_64
#EXCLUDE_ARCHS += linux-ppc64e6500

APPSRC:=src/main/c++/nds
APPINC:=src/main/c++/include

USR_INCLUDES += -I$(where_am_I)$(APPINC)
USR_CXXFLAGS += -std=c++11
USR_CXXFLAGS += -DNDS3_DLL -DNDS3_DLL_EXPORTS


HEADERS += $(wildcard $(APPINC)/nds3/*.h)
HEADERS += $(wildcard $(APPINC)/nds3/impl/*.h)
SOURCES += $(wildcard $(APPSRC)/*.cpp)

# db rule is the default in RULES_E3, so add the empty one
db:
#
.PHONY: vlibs
vlibs:
#